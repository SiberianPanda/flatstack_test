class Sequence
  def current_row
    @current_row.join('')
  end

  def initialize(row: '1')
    @current_row = row.split('')
  end

  def next
    row = ''
    counter = 0
    prev = nil
    len = @current_row.length
    @current_row.each_with_index do |curr, inx|
      if curr != prev
        row += "#{counter}#{prev}" if inx != 0
        prev = curr
        counter = 1
      else
        counter += 1
      end
      row += "#{counter}#{prev}" if (len - 1) == inx
    end
    @current_row = row.split('')
  end

  def prev
    row = ''
    counter = nil
    @current_row.each_with_index do |curr, inx|
      counter = curr if inx.even?
      row += curr * counter.to_i if inx.odd?
    end
    @current_row = row.empty? ? ['1'] : row.split('')
  end
end

# seq = Sequence.new
# 6.times do
#   seq.next
#   puts seq.current_row
# end
# 6.times do
#   seq.prev
#   puts seq.current_row
# end
# 6.times do
#   seq.next
#   puts seq.current_row
# end
