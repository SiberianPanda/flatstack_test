require_relative '../sequence'
RSpec.describe 'sequence' do
  context 'when sequence created with default value' do
    let(:seq) { Sequence.new }
    context 'when first 5 value' do
      let(:template) do
        <<-TEMPLATE
1
11
21
1211
111221
        TEMPLATE
      end
      it 'should return values as templates' do
        result = [seq.current_row]
        result << Array.new(4).map { seq.next; seq.current_row }
        result = result.join("\n") + "\n"
        expect(result).to eq(template)
      end
    end
  end
  context 'when sequence created with some value' do
    let(:seq) { Sequence.new(row: '111221') }
    context 'when try get previus value' do
      let(:template) { '1211' }
      it 'should return template' do
        seq.prev
        expect(seq.current_row).to eq(template)
      end
    end
  end
end
